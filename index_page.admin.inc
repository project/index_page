<?php

/**
 * @file
 * Provides a blank page to put stuff (blocks, views..) on as a Frontpage.
 */

/**
 * Implements system_settings_form().
 */
function index_page_admin_form_settings($form, &$form_state) {
  $form = array();
  $form['site_frontpage'] = array(
    '#type' => 'textfield',
    '#title' => t('Default front page'),
    '#default_value' => (variable_get('site_frontpage') != 'node' ? drupal_get_path_alias(variable_get('site_frontpage', 'node')) : 'index'),
    '#size' => 40,
    '#description' => t('Specify a relative URL to display as the front page.  Use "index" for this module.'),
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
  );

  $desc = t('For SEO goodness give your front page an HTML title tag.');
  if (!module_exists('variable')) {
    $attr = array(
      'external' => TRUE,
      'attributes' => array('target' => '_blank'),
    );
    $desc .= '  ' . t('Variable is translation ready (Using project !link).', array(
      '!link' => l(t('Variable'), 'https://www.drupal.org/project/variable', $attr),
    ));
  }
  $form['index_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Index Page title'),
    '#required' => TRUE,
    '#default_value' => variable_get('index_page_title', 'Welcome to our site...'),
    '#description' => $desc,
  );

  $form['help'] = array(
    '#weight' => 500,
    '#type' => 'markup',
    '#markup' => t('"Default front page" is the same on the !link page.', array('!link' => l(t('Site information'), 'admin/config/system/site-information'))),
  );

  return system_settings_form($form);
}
