CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 Makes the path /index to be used as a static Frontpage for your site.
 
 * Requirements
 Menu, drupal core
 
 * Recommended modules
 Variable, for title translation
 https://www.drupal.org/project/node_page_disable
 
 * Installation
 Install in usual drupal module paths
 
 * Configuration
 /admin/appearance/index-page
 
 * Troubleshooting
 make sure Default front page on Configuration page (link above) set to 'index'
 
 * FAQ
 No notes
 
 * Maintainers
 SKAUGHT drupal.org/u/SKAUGHT
 
 